function check(requestdetails) {
    var curr = chrome.windows.getCurrent(getInfo);
    var yn = curr.confirm("♫ Ertu nú alveg viss um? ♫");
    if(!yn){
        return {cancel: true};
    }
    if(yn){
        chrome.webRequest.onBeforeRequest.removeListener(check);
        setTimeout(function() {addlistener(); }, 5000);
        return {cancel: false};
    }
}

function addlistener(){
    chrome.webRequest.onBeforeRequest.addListener(check,{ urls: ["*://www.mbl.is/smartland*", "*://www.dv.is/*"] },["blocking"]);
}

addlistener();