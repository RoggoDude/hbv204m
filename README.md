A chrome extension that aims to allow the user to rethink their decision of clicking a hyperlink that directs them to "Smartland" or "DV".

To install dependent packages, have node.js installed and type "npm install" into the terminal and run.

To run automated testing, finish installing dependacies and type "npm run test" into the terminal and run.