const sum = require("./funcstotest");

test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);
});

test('adds 0 + 10 to equal 10', () => {
    expect(sum(-10, 10)).toBe(0);
  });

